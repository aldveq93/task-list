function ctrlScript(){
    const doc = document;
    // Function to validate if the task list is empty
    validateEmptyTaskList();
    // Event that open the modal window to add a new task to the list
    doc.getElementById('openModal').addEventListener('click', callNewTaskModal);
}

// Function to build and activate the modal window to add new tasks
function callNewTaskModal() {
    const doc = document;
    let bodyBuild = modalBody();
    doc.getElementById('containerModal').innerHTML = bodyBuild.closeIcon + bodyBuild.formNewTask;
    doc.getElementById('modal').classList.add('activeModal');
    doc.getElementById('btnAddTask').addEventListener('click', addNewTask);
    doc.getElementById('closeModalTask').addEventListener('click', closeTaskModal);
}

// Function to destroy the modal window content
function destroyModalContent(){
    const doc = document;
    doc.getElementById('containerModal').innerHTML = '';
}

// Function to return an object with the modal window content
function modalBody(){
    let newTask = {
        closeIcon : `<span class='closeModal closeModal--mod' id='closeModalTask'>X</span>`,
        formNewTask : `<form class="form-task">
        <h1 class="title" id="titleModal">Add a new task!</h1>
        <div class="form-group">
            <label for="" class="task-label">Date</label>
            <input type="date" id="date" class="inputNewTask">
        </div>
        <div class="form-group">
            <label for="" class="task-label">Hour</label>
            <input type="time" id="hour" class="inputNewTask">
        </div>
        <div class="form-group">
            <label for="" class="task-label">New task</label>
            <input type="text" id="newTask" class="inputNewTask" placeholder="Type a new task">
        </div>
        <div class="form-group">
            <button type="button" class="btnAddTask" id="btnAddTask">Add task</button>
        </div>
    </form>`,
    formTaskManagement : `<form class="form-task">
    <h1 class="title">What do you whant to do with the task?</h1>
    <div class="form-group form-group--flex">
        <button type="button" class="btnAddTask btnAddTask--mod" id="btnRemoveTask">Remove</button>
        <button type="button" class="btnAddTask btnAddTask--mod" id="btnEditTask">Edit</button>
        <button type="button" class="btnAddTask btnAddTask--mod" id="btnCompletedTask">Completed</button>
    </div>
</form>`
    };

    return newTask;
}

// Function to add a new task to the list
function addNewTask() {
    const doc = document;

    // Declaración de variables que traen el contenido de una tarea
    let date = doc.getElementById('date').value,
        hour = doc.getElementById('hour').value,
        taskContent = doc.getElementById('newTask').value;
    
    // Creación de una etiqueta <p></p>
    const p = doc.createElement('p'),
    taskListContainer = doc.getElementById('task-container');
    // Creación de etiquetas <div></div> y <span></span>
    let fatherDivContainer = doc.createElement('div'),
        firstChildContainer =  doc.createElement('div'),
        lastChildContainer = doc.createElement('div'),
        spanDate = doc.createElement('span'),
        spanHour = doc.createElement('span');

    if (date == '' || hour == '' || taskContent == '') {
        feedbackMessage('error','Please, fill out all the inputs!');
    } else {
        // Definición del div contenedor padre de la tarea con fecha y hora    
        fatherDivContainer.className = 'task-list__item';
        // fatherDivContainer.setAttribute('data-task','selected');
        // Definición del primer contenedor que tiene la tarea
        firstChildContainer.className = 'task-list__content-container1';
        p.textContent = taskContent;
        firstChildContainer.appendChild(p);

        // Definición del segundo contenedor que tiene fecha y hora de la nueva tarea
        lastChildContainer.className = 'task-list__content-container2';
        spanDate.textContent = date;
        spanDate.className = 'date';
        lastChildContainer.appendChild(spanDate);

        spanHour.textContent = hour;
        spanHour.className = 'hour';
        lastChildContainer.appendChild(spanHour);

        // Agregando ambos div al div padre 
        fatherDivContainer.appendChild(firstChildContainer);
        fatherDivContainer.appendChild(lastChildContainer);

        // Agregando la nueva tarea a la lista
        taskListContainer.appendChild(fatherDivContainer);

        feedbackMessage('success','Task added!');
        closeTaskModal();
    }  
    
    validateEmptyTaskList();
    showTaskManagement();      
}

// Function that gives feedback message to the user, if the task list is empty
function validateEmptyTaskList(){
    const doc = document,
          taskListContainer = doc.getElementById('task-container');

    if (taskListContainer.childElementCount > 0){
        doc.getElementById('emoji').classList.add('feedback-none');
        doc.getElementById('feedbackEmptyTaskList').classList.add('feedback-none');
        doc.getElementById('taskListTitle').classList.add('showContainers');
        taskListContainer.classList.add('showContainers');
    } else{
        doc.getElementById('emoji').classList.remove('feedback-none');
        doc.getElementById('feedbackEmptyTaskList').classList.remove('feedback-none');
        doc.getElementById('taskListTitle').classList.remove('showContainers');
        taskListContainer.classList.remove('showContainers');
    }
}

// Function to manage the tasks 
function showTaskManagement(){
    const doc = document,
          selectedTasks = doc.querySelectorAll('.task-list__item');

    selectedTasks.forEach((task, index) =>{
        task.addEventListener('click', (e) => {

            e.stopImmediatePropagation();
            
            if (task.classList.contains('complete-task')){
                feedbackMessage('error','The task is already completed!');
            } else {
                callManagamentTaskModal();
                
                doc.getElementById('btnRemoveTask').addEventListener('click', (e) => {
                    e.stopImmediatePropagation();
                    selectedTasks[index].remove();
                    validateEmptyTaskList();
                    closeTaskModal();
                    feedbackMessage('success','Task removed!');
                });

                doc.getElementById('btnEditTask').addEventListener('click', (e) => {
                    e.stopImmediatePropagation();
                    chargeDataToModifyTask(selectedTasks[index]);
                });

                doc.getElementById('btnCompletedTask').addEventListener('click', (e) =>{
                    e.stopImmediatePropagation();
                    selectedTasks[index].classList.add('complete-task');
                    closeTaskModal();
                    feedbackMessage('success','Task completed!');
                });
            }
        }); 
    });
}

// Function to load the data from a specific task to the modal window to be edited
function chargeDataToModifyTask(modifyTask){
    const doc = document;
    let task = modifyTask.children[0].children[0].textContent,
        date = modifyTask.children[1].children[0].textContent,
        hour = modifyTask.children[1].children[1].textContent,
        bodyBuild = modalBody();

    destroyModalContent();

    doc.getElementById('containerModal').innerHTML = bodyBuild.closeIcon + bodyBuild.formNewTask;
    doc.getElementById('titleModal').textContent = 'Modify Task';
    doc.getElementById('date').value = date;
    doc.getElementById('hour').value = hour;
    doc.getElementById('newTask').value = task;
    doc.getElementById('closeModalTask').addEventListener('click', closeTaskModal);
    doc.getElementById('btnAddTask').textContent = 'Modify';
    doc.getElementById('btnAddTask').addEventListener('click', () => {

        if (doc.getElementById('date').value == '' || doc.getElementById('hour').value == '' || doc.getElementById('newTask').value == ''){
            feedbackMessage('error','Please, fill out all the inputs!');
        } else {
            modifyTask.children[1].children[0].textContent = doc.getElementById('date').value;

            modifyTask.children[1].children[1].textContent = doc.getElementById('hour').value;

            modifyTask.children[0].children[0].textContent = doc.getElementById('newTask').value;

            closeTaskModal();
            feedbackMessage('success','Task modified!');
        }
    });
    doc.getElementById('modal').classList.add('activeModal');
}

// Fuction that build and activate the modal window to manage the tasks
function callManagamentTaskModal() {
    const doc = document;
    let bodyBuild = modalBody();
    doc.getElementById('containerModal').innerHTML = bodyBuild.closeIcon + bodyBuild.formTaskManagement;
    doc.getElementById('modal').classList.add('activeModal');
    doc.getElementById('closeModalTask').addEventListener('click', closeTaskModal);
}

// Function that close the modal window, after a new task was added to the task list 
function closeTaskModal() {
    const doc = document;
    destroyModalContent();
    doc.getElementById('modal').classList.remove('activeModal');
}

// Function to indicates a type of feedback message to the user 
function feedbackMessage(typeError,message){
    const doc = document;
    let feedback = doc.getElementById('feedback-message');

    feedback.textContent = message;
    
    if (typeError == 'success') {
        feedback.classList.add('feedback-message--bgSuccess');
    } else {
        feedback.classList.add('feedback-message--bgError');
    }

    feedback.classList.add('active-feedback-message');

    setTimeout(cleanFeedback,3000);
}

// Function to clean the feedback message
function cleanFeedback() {
    const doc = document;
    doc.getElementById('feedback-message').textContent = '';
    doc.getElementById('feedback-message').classList.remove('feedback-message--bgSuccess');
    doc.getElementById('feedback-message').classList.remove('feedback-message--bgError');   
    doc.getElementById('feedback-message').classList.remove('active-feedback-message'); 
}

ctrlScript();

